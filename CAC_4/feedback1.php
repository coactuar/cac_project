<?php
require_once 'functions.php';

$succ = false;
$errors = [];

$q01 = '';
$q02 = '';
$q03 = '';
$q04 = '';
$q05 = '';
$q06 = '';
$q07 = '';
$q08 = '';
$q09 = '';
$q10 = '';
$q11 = '';
$q12 = '';
$q13 = '';
$q14 = '';
$q15 = '';
$q16 = '';
$q17 = '';
$q18 = '';
$q19 = '';
$q20 = '';
$q21 = '';
$q22 = '';
$q23 = '';
$q24 = '';
$q25 = '';
$q26 = '';
$q27 = '';
$q28 = '';
$q29 = '';
$q30 = '';
$q31 = '';
$q32 = '';
$q33 = '';
$q34 = '';
$q35 = '';
$q36 = '';
$userid = '';

if (isset($_POST['fbsub-btn'])) {

    if (
        empty($_POST['q01']) ||
        empty($_POST['q02']) ||
        empty($_POST['q03']) ||
        empty($_POST['q04']) ||
        empty($_POST['q05']) ||
        empty($_POST['q06']) ||
        empty($_POST['q07']) ||
        empty($_POST['q08']) ||
        empty($_POST['q09']) ||
        empty($_POST['q10']) ||
        empty($_POST['q11']) ||
        empty($_POST['q12']) ||
        empty($_POST['q13']) ||
        empty($_POST['q14']) ||
        empty($_POST['q15']) ||
        empty($_POST['q16']) ||
        empty($_POST['q17']) ||
        empty($_POST['q18']) ||
        empty($_POST['q19']) ||
        empty($_POST['q20']) ||
        empty($_POST['q21']) ||
        empty($_POST['q22']) ||
        empty($_POST['q23']) ||
        empty($_POST['q24']) ||
        empty($_POST['q25']) ||
        empty($_POST['q26']) ||
        empty($_POST['q27']) ||
        empty($_POST['q28']) ||
        empty($_POST['q29']) ||
        empty($_POST['q30']) ||
        empty($_POST['q31']) ||
        empty($_POST['q32']) ||
        empty($_POST['q33']) ||
        empty($_POST['q34']) ||
        empty($_POST['q35']) ||
        empty($_POST['q36'])

    ) {
        $errors['reply'] = 'Please answer all questions.';
    }
    if (isset($_POST['userid'])) {
        $userid = $_POST['userid'];
    } else {
        header('location: ./');
    }

    if (isset($_POST['q01'])) {
        $q01 = $_POST['q01'];
    }
    if (isset($_POST['q02'])) {
        $q02 = $_POST['q02'];
    }
    if (isset($_POST['q03'])) {
        $q03 = $_POST['q03'];
    }
    if (isset($_POST['q04'])) {
        $q04 = $_POST['q04'];
    }
    if (isset($_POST['q05'])) {
        $q05 = $_POST['q05'];
    }
    if (isset($_POST['q06'])) {
        $q06 = $_POST['q06'];
    }
    if (isset($_POST['q07'])) {
        $q07 = $_POST['q07'];
    }
    if (isset($_POST['q08'])) {
        $q08 = $_POST['q08'];
    }
    if (isset($_POST['q09'])) {
        $q09 = $_POST['q09'];
    }
    if (isset($_POST['q10'])) {
        $q10 = $_POST['q10'];
    }
    if (isset($_POST['q11'])) {
        $q11 = $_POST['q11'];
    }
    if (isset($_POST['q12'])) {
        $q12 = $_POST['q12'];
    }
    if (isset($_POST['q13'])) {
        $q13 = $_POST['q13'];
    }
    if (isset($_POST['q14'])) {
        $q14 = $_POST['q14'];
    }
    if (isset($_POST['q15'])) {
        $q15 = $_POST['q15'];
    }
    if (isset($_POST['q16'])) {
        $q16 = $_POST['q16'];
    }
    if (isset($_POST['q17'])) {
        $q17 = $_POST['q17'];
    }
    if (isset($_POST['q18'])) {
        $q18 = $_POST['q18'];
    }
    if (isset($_POST['q19'])) {
        $q19 = $_POST['q19'];
    }
    if (isset($_POST['q20'])) {
        $q20 = $_POST['q20'];
    }
    if (isset($_POST['q21'])) {
        $q21 = $_POST['q21'];
    }
    if (isset($_POST['q22'])) {
        $q22 = $_POST['q22'];
    }
    if (isset($_POST['q23'])) {
        $q23 = $_POST['q23'];
    }
    if (isset($_POST['q24'])) {
        $q24 = $_POST['q24'];
    }
    if (isset($_POST['q25'])) {
        $q25 = $_POST['q25'];
    }
    if (isset($_POST['q26'])) {
        $q26 = $_POST['q26'];
    }
    if (isset($_POST['q27'])) {
        $q27 = $_POST['q27'];
    }
    if (isset($_POST['q28'])) {
        $q28 = $_POST['q28'];
    }
    if (isset($_POST['q29'])) {
        $q29 = $_POST['q29'];
    }
    if (isset($_POST['q30'])) {
        $q30 = $_POST['q30'];
    }
    if (isset($_POST['q31'])) {
        $q31 = $_POST['q31'];
    }
    if (isset($_POST['q32'])) {
        $q32 = $_POST['q32'];
    }
    if (isset($_POST['q33'])) {
        $q33 = $_POST['q33'];
    }
    if (isset($_POST['q34'])) {
        $q34 = $_POST['q34'];
    }
    if (isset($_POST['q35'])) {
        $q35 = $_POST['q35'];
    }
    if (isset($_POST['q36'])) {
        $q36 = $_POST['q36'];
    }



    if (count($errors) == 0) {
        $fb = new Feedback();
        $fb->__set('user_id', $userid);
        $fb->__set('q01', $_POST['q01']);
        $fb->__set('q02', $_POST['q02']);
        $fb->__set('q03', $_POST['q03']);
        $fb->__set('q04', $_POST['q04']);
        $fb->__set('q05', $_POST['q05']);
        $fb->__set('q06', $_POST['q06']);
        $fb->__set('q07', $_POST['q07']);
        $fb->__set('q08', $_POST['q08']);
        $fb->__set('q09', $_POST['q09']);
        $fb->__set('q10', $_POST['q10']);
        $fb->__set('q11', $_POST['q11']);
        $fb->__set('q12', $_POST['q12']);
        $fb->__set('q13', $_POST['q13']);
        $fb->__set('q14', $_POST['q14']);
        $fb->__set('q15', $_POST['q15']);
        $fb->__set('q16', $_POST['q16']);
        $fb->__set('q17', $_POST['q17']);
        $fb->__set('q18', $_POST['q18']);
        $fb->__set('q19', $_POST['q19']);
        $fb->__set('q20', $_POST['q20']);
        $fb->__set('q21', $_POST['q21']);
        $fb->__set('q22', $_POST['q22']);
        $fb->__set('q23', $_POST['q23']);
        $fb->__set('q24', $_POST['q24']);
        $fb->__set('q25', $_POST['q25']);
        $fb->__set('q26', $_POST['q26']);
        $fb->__set('q27', $_POST['q27']);
        $fb->__set('q28', $_POST['q28']);
        $fb->__set('q29', $_POST['q29']);
        $fb->__set('q30', $_POST['q30']);
        $fb->__set('q31', $_POST['q31']);
        $fb->__set('q32', $_POST['q32']);
        $fb->__set('q33', $_POST['q33']);
        $fb->__set('q34', $_POST['q34']);
        $fb->__set('q35', $_POST['q35']);
        $fb->__set('q36', $_POST['q36']);

        $subFeedback = $fb->submitFeedback();

        //var_dump($subFeedback);

        if ($subFeedback['status'] == 'success') {
            $succ = true;
        } else {
            $errors['msg'] = $subFeedback['message'];
        }
    }
}

?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $event_title ?></title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

    <div class="container-fluid">
        <div class="row bg-white">
            <div class="col-12 p-0">
                <img src="assets/img/banner-feedback.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row bg-white color-grey py-2">
            <div class="col-12 col-md-8 mx-auto">
                <h5 class="reg-title">
                    Thank you for attending OSTEOKONNECT
                </h5>
                <p>Please let us know about your experience regarding the program, faculty, and its relevace for clinical practice.</p>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 mx-auto">
                <?php if (!$succ) { ?>
                    <div id="register-area">
                        <?php
                        if (count($errors) > 0) : ?>
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    <?php foreach ($errors as $error) : ?>
                                        <li>
                                            <?php echo $error; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif;
                        ?>
                        <form method="POST">
                            <input type="hidden" id="userid" name="userid" class="input" value="<?= $_SESSION['userid'] ?>" required>

                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <th align="left">Please rate the following</td>
                                            <th width="150">Strongly Agree</td>
                                            <th width="150">Strongly Disagree</td>
                                            <th width="100">Agree</td>
                                            <th width="100">Disagree</td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">The content will be useful in my practice.</td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Appropriate time was allocated for discussion.</td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">The educational design/format supported my learning.</td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Would recommend this course to a colleague.</td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Please rate the faculty's quality of teaching.</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <td align="left"><b>Faculty</b></td>
                                            <th width="150">Excellent</th>
                                            <th width="100">Good</th>
                                            <th width="100">Avergae</th>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Altamash Shaikh</td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Ambrish Mithal</td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Ameya Joshi</td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Hema Divakar</td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Hemant Kalyan</td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. J.K. Lee (Mal)</td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Krishna Seshadri</td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Laun Tang Ching (Sing)</td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Manju Chandran (Sing)</td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Manoj Chadha</td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Michael Holick (USA)</td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Pramod Gandhi</td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Rajesh Malhotra</td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Ram Chaddha</td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Sanjay Bhadada</td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Shelha Shaikh</td>
                                            <td><input type="radio" name="q20" <?= ($q20 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q20" <?= ($q20 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q20" <?= ($q20 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Thomas Paul</td>
                                            <td><input type="radio" name="q21" <?= ($q21 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q21" <?= ($q21 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q21" <?= ($q21 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Uday Phadke</td>
                                            <td><input type="radio" name="q22" <?= ($q22 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q22" <?= ($q22 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q22" <?= ($q22 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Usha Sriram (Chicago)</td>
                                            <td><input type="radio" name="q23" <?= ($q23 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q23" <?= ($q23 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q23" <?= ($q23 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr. Vaishali Deshmukh</td>
                                            <td><input type="radio" name="q24" <?= ($q24 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q24" <?= ($q24 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q24" <?= ($q24 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Please rate the topics relevance to your clinical practice.</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <td align="left"><b>Topics</b></td>
                                            <th width="150">Relevant </td>
                                            <th width="150">Partly Relevant </td>
                                            <th width="150">Not Relevant </td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Overview & Diagnosis of Osteoporosis</td>
                                            <td><input type="radio" name="q25" <?= ($q25 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q25" <?= ($q25 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q25" <?= ($q25 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Postmenopausal Osteoporosis</td>
                                            <td><input type="radio" name="q26" <?= ($q26 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q26" <?= ($q26 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q26" <?= ($q26 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Management - Non pharmac</td>
                                            <td><input type="radio" name="q27" <?= ($q27 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q27" <?= ($q27 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q27" <?= ($q27 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">The D-Lightful Vitamin D: Noncalcemic Health Benefits from Birth until Death</td>
                                            <td><input type="radio" name="q28" <?= ($q28 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q28" <?= ($q28 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q28" <?= ($q28 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Management - Pharmac (current)</td>
                                            <td><input type="radio" name="q29" <?= ($q29 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q29" <?= ($q29 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q29" <?= ($q29 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Fragility fractures & FLS</td>
                                            <td><input type="radio" name="q30" <?= ($q30 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q30" <?= ($q30 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q30" <?= ($q30 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Sequential & Combination therapy</td>
                                            <td><input type="radio" name="q31" <?= ($q31 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q31" <?= ($q31 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q31" <?= ($q31 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">New therapies on the horizon</td>
                                            <td><input type="radio" name="q32" <?= ($q32 == 'Relevant') ? 'checked' : '' ?> value="Relevant"></td>
                                            <td><input type="radio" name="q32" <?= ($q32 == 'Partly Relevant') ? 'checked' : '' ?> value="Partly Relevant"></td>
                                            <td><input type="radio" name="q32" <?= ($q32 == 'Not Relevant') ? 'checked' : '' ?> value="Not Relevant"></td>
                                        </tr>


                                    </table>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Please rate the Overall Program</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <th width="25%">Excellent</th>
                                            <th width="25%">Good</th>
                                            <th width="25%">Average</th>
                                            <th width="25%">Below Average</th>
                                        </tr>
                                        <tr align="center">
                                            <td><input type="radio" name="q33" <?= ($q33 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q33" <?= ($q33 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q33" <?= ($q33 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q33" <?= ($q33 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Describe your level of practice</strong>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <ul class="list-unstyled">
                                        <li><input type="radio" name="q34" <?= ($q34 == 'Resident/In-training') ? 'checked' : '' ?> value="Resident/In-training"> Resident/In-training</li>
                                        <li><input type="radio" name="q34" <?= ($q34 == '0-5 years of practice') ? 'checked' : '' ?> value="0-5 years of practice"> 0-5 years of practice</li>
                                        <li><input type="radio" name="q34" <?= ($q34 == '5-10 years of practice') ? 'checked' : '' ?> value="5-10 years of practice"> 5-10 years of practice</li>
                                        <li><input type="radio" name="q34" <?= ($q34 == '10-20 years of practice') ? 'checked' : '' ?> value="10-20 years of practice"> 10-20 years of practice</li>
                                        <li><input type="radio" name="q34" <?= ($q34 == '>20 years of practice') ? 'checked' : '' ?> value=">20 years of practice"> >20 years of practice</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>What is your primary specialty area?</strong>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <ul class="list-unstyled">
                                        <li><input type="radio" name="q35" <?= ($q35 == 'General orthopedic surgeon') ? 'checked' : '' ?> value="General orthopedic surgeon"> General orthopedic surgeon</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Shoulder and Elbow') ? 'checked' : '' ?> value="Shoulder and Elbow"> Shoulder and Elbow</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Spine') ? 'checked' : '' ?> value="Spine"> Spine</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Sports Medicine') ? 'checked' : '' ?> value="Sports Medicine"> Sports Medicine</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Pediatrics') ? 'checked' : '' ?> value="Pediatrics"> Pediatrics</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Hip and Knee') ? 'checked' : '' ?> value="Hip and Knee"> Hip and Knee</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Foot and Ankle') ? 'checked' : '' ?> value="Foot and Ankle"> Foot and Ankle</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Endocrinology') ? 'checked' : '' ?> value="Endocrinology"> Endocrinology</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Diabetology') ? 'checked' : '' ?> value="Diabetology"> Diabetology</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Rheumatology') ? 'checked' : '' ?> value="Rheumatology"> Rheumatology</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Obstetrics') ? 'checked' : '' ?> value="Obstetrics"> Obstetrics</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Gynecology') ? 'checked' : '' ?> value="Gynecology"> Gynecology</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'IVF Specialization') ? 'checked' : '' ?> value="IVF Specialization"> IVF Specialization</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'PCOS') ? 'checked' : '' ?> value="PCOS"> PCOS</li>
                                        <li><input type="radio" name="q35" <?= ($q35 == 'Consulting Physician') ? 'checked' : '' ?> value="Consulting Physician"> Consulting Physician</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-12">
                                    <strong>Any other suggestions</strong>
                                    <br>
                                    <textarea name="q36" id="q36" rows="4" class="input"><?= $q36 ?></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <input type="submit" name="fbsub-btn" id="btnSubmit" class="btn btn-primary" value="Submit">
                            </div>



                        </form>
                    </div>
                <?php } else { ?>
                    <div id="registration-confirmation">
                        <div class="alert alert-success">
                            Thanks for giving us your valuable feedback!<br>
                        </div>

                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="row pt-5 bg-white">
            <div class="col-12 p-0">
                <img src="assets/img/bottm-banner-feedback.jpg" class="img-fluid" alt="">
            </div>
        </div>

        <script src="//code.jquery.com/jquery-latest.js"></script>
        <?php require_once 'ga.php';  ?>
        <?php require_once 'footer.php';  ?>